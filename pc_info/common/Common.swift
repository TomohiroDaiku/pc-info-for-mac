//
//  command.swift
//  pc_info
//
//  Created by 大工智博 on 2017/11/26.
//  Copyright © 2017年 大工智博. All rights reserved.
//

import Foundation

class Command {
    init(cmd:String , arguments:String=""){
        let task:Process = Process()
        task.launchPath = cmd;
        //配列
        task.arguments  = [arguments]
        
        let pipe:Pipe = Pipe()
        task.standardOutput = pipe
        task.launch()
//        var output:NSData = pipe.fileHandleForReading.readDataToEndOfFile() as NSData
//        let outputStr:NSString = NSString(data:output as Data,encoding:String.Encoding.utf8.rawValue)!
//        print(outputStr)
        
    }
}

class Terminal {
    init(){
        
    }
    
    static func filelist(){
        let manager = FileManager.default
        
        do {
            let list = try manager.contentsOfDirectory(atPath: "/bin")
            for path in list {
                print(path as String)
            }
        } catch  {
            print("cant read file list.")
        }
        
        
    }
}

//
//  AppDelegate.swift
//  pc_info
//
//  Created by 大工智博 on H29/09/24.
//  Copyright © 平成29年 大工智博. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}


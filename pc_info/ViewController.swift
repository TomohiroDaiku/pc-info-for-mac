//
//  ViewController.swift
//  pc_info
//
//  Created by 大工智博 on H29/09/24.
//  Copyright © 平成29年 大工智博. All rights reserved.
//

import Cocoa
import CFNetwork

class ViewController: NSViewController, NSTableViewDataSource {
    
    //基本情報タブ
    @IBOutlet weak var txt_user_nm: NSTextField!
    @IBOutlet weak var txt_host_name: NSTextField!
    @IBOutlet weak var txt_ip: NSTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //基本情報
        let pInfo:ProcessInfo = ProcessInfo()
        txt_host_name.stringValue = pInfo.hostName + "(" + pInfo.operatingSystemVersionString + ")"
        if #available(OSX 10.12, *) {
            txt_user_nm.stringValue = pInfo.fullUserName + "(" + pInfo.userName + ")"
        }
        
        setIP()
        
        loadAppList()
        //Command(cmd:"/bin/ls",arguments:"~/")
        Terminal.filelist()
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


    func loadAppList(){
        
    }
    
    let programs = ["Swift", "C", "Java", "JavaScript", "PHP", "Python"]
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        return programs.count
    }
    
    func tableView(_ tableView: NSTableView, objectValueFor tableColumn: NSTableColumn?, row: Int) -> Any? {
        return programs[row]
    }
    
    func setIP(){
        //let pool = NSAutoreleasePool()
        // ローカルノードに関連するNSHostを取得
        var local: Host?
        local = Host.current() as? Host
        // IPアドレス一覧を取得
        let addresses = local?.addresses
        // 取得したIPアドレス一覧を表示
        if((addresses?.count)! > 0){
            txt_ip.stringValue = (addresses?[0])!
        }
    }
    
    
}

